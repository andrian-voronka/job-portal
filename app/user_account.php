<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class user_account extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'users';
    
     protected $fillable = [
        'id', 'user_type_id','first_name','last_name','city','phone_no' ,'Email','password','dob','gender','registration_date','education','Experience','skills','business_stream' 
    ];
}
