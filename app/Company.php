<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Company extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Companies';
    
     protected $fillable = [
        'id', 'company_name' ,'profile','business_stream_id','website_url'
    ];
}
