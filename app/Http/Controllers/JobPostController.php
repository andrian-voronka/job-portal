<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\JobPost;
use App\User;
use App\Application;
use App\Employee;
use DB;
 
class JobPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $path = public_path() . "\canadian_cities.json";
        $cities = file_get_contents($path); 
        $data['citys'] = json_decode($cities);
        //$data['applys'] = DB::table('Application')->join('JobPost','JobPost._id','Application.job_id')->join('user','users._id','Application.user_id')->get();

        //$data['applys'] = Application::get();

        $data['applys'] = Application::get();
        $data['users'] = User::where('role','user')->get();
        $data['jobs'] = JobPost::get();
        $data['employees'] = Employee::get();
        $data['currentUser'] = Auth::user();
        return view('postJob',$data);
    }
    
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['status'] = "1";
        $data['user_id'] = $user->_id;
        JobPost::create($data);
        return redirect('postJob');
    }  
}