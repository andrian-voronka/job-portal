<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Http\Request;
use Auth;
use App\User;
 
class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function getUserProfile()
    {
        $data['user'] = Auth::user();
        return view('user/profile',$data);
    }

    public function updateUserProfile(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        User::where("_id",$user->_id)->update($data);
        return redirect('profile');
    }  
 
    public function ajaxRequestPost(Request $request)

    {
         try{
          $Email = $request->get('Email');

          $users =  user_account::all();

        return response()->json(['success'=>   $users]);
        }
         catch(Exception $e)
         {
              Debugbar::addException($e);
         }

    }

    public function updateProfile(Request $request)
    {
        try{
         $Email = $request->get('Email');
         
         $userAccount1= user_account::where('Email',$Email);
        
         $userAccount1->delete();
         $userAccount = new user_account();
           
         $userAccount->user_type_id = 1;
         $userAccount->Email = $request->get('Email');
         $userAccount->password = $request->get('password');        
         $userAccount->dob = $request->get('dob');        
         $userAccount->gender = $request->get('gender');        
         $userAccount->registration_date = date('m/d/Y');         
         $userAccount->first_name = $request->get('first_name');        
         $userAccount->last_name = $request->get('last_name');        
         $userAccount->city = $request->get('city');        
         $userAccount->city = $request->get('phone_no');      
        
         $userAccount->save();
         return response()->json(['success'=>   $userAccount]);
          
        }
      catch(Exception $e)
         {
              return response()->json(['success'=>   $e]);
         }
    }

}