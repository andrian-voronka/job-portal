<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Application;
use App\Employee;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['user_id'] = $user->_id;
        Application::create($data);
        error_log('applied');
        return response()->json(["success"=> "Applied Successfully"]);
    }

    public function addEmployee(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $data['company_id'] = $user->_id;
        Employee::create($data);
        error_log('hired');
        return "User added Successfully";
    }
    
    private function checkIfMatches(Request $data)
    {
        echo $data;
    }
}