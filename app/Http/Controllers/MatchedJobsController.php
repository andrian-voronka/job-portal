<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\JobPost;
use App\User;
use App\Application;
use App\Employee;
use DB;
 
class MatchedJobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
        $path = public_path() . "\canadian_cities.json";
        $cities = file_get_contents($path); 
        $data['citys'] = json_decode($cities);
        $data['applications'] = Application::get();
        $data['users'] = User::where('role','company')->get();
        $data['jobs'] = JobPost::get();
        $data['employees'] = Employee::get();
        $data['currentUser'] = Auth::user();
        
        return view('matchedjobs',$data);
    }
}