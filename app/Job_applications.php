<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Job_applications extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Job_Posts';
    
     protected $fillable = [
        'id', 'user_id' ,'company_id','job_id','date_applied'
    ];
}
