<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class JobPost extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'JobPosts';
     protected $guarded = [];
     protected $primarykey = "_id";
    
     protected $fillable = [
        'user_id' ,
        'title',
        'description',
        'city',
        'skills',
        'business_stream',
        'experience',
        'status'
    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
