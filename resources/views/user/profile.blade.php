@extends('theme.default')
@section('content')
    <div class="container" style="padding: 20px">
        <div style="padding: 30px">
            <h5 style="text-align: center;">Update your profile</h5>
        </div>

        <div  style="padding-bottom: 50px">
            <form method="POST" action="{{ url('profile') }}">
                <div class="row ">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First Name *" name="first_name" id="first_name" value="{{$user->first_name}}"  required="" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last Name *" name="last_name" id="last_name" value="{{$user->last_name}}"  required="" />
                        </div>
                        <!--<div class="form-group">
                            <input type="password" class="form-control" placeholder="Password *" name="password" id="password"  value="" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control"  placeholder="Confirm Password *" name="retypePassword" value="" />
                        </div> -->
                        <div class="form-group">
                            <input class="date form-control" name="dob" type="date" value="{{$user->dob}}" required="">
                        </div>
                        <div class="form-group">
                            <fieldset>
                                <select class="form-control dropdown" id="business_stream" name="business_stream" required="">
                                    <option value="" selected="selected" disabled="disabled">Business Stream</option>
                                    <option value="Aerospace" @if($user->business_stream=="Aerospace") selected @endif>Aerospace </option>
                                    <option value="Transport" @if($user->business_stream=="Transport") selected @endif>Transport </option>
                                    <option value="Computer ,IT" @if($user->business_stream=="Computer ,IT") selected @endif>Computer ,IT</option>
                                    <option value="Telecommunication" @if($user->business_stream=="Telecommunication") selected @endif>Telecommunication </option>
                                    <option value="Agriculture" @if($user->business_stream=="Agriculture") selected @endif>Agriculture </option>
                                    <option value="Construction" @if($user->business_stream=="Construction") selected @endif>Construction  </option>
                                    <option value="Education" @if($user->business_stream=="Education") selected @endif>Education  </option>
                                    <option value="Pharmaceutical" @if($user->business_stream=="Pharmaceutical") selected @endif>Pharmaceutical</option>
                                    <option value="Food" @if($user->business_stream=="Food") selected @endif>Food</option>
                                    <option value="Health care" @if($user->business_stream=="Health care") selected @endif>Health care</option>
                                    <option value="Hospitality" @if($user->business_stream=="Hospitality") selected @endif>Hospitalit</option>
                                    <option value="News Media" @if($user->business_stream=="News Media") selected @endif>News Medi</option>
                                    <option value="Energy" @if($user->business_stream=="Energy") selected @endif>Energy</option>
                                    <option value="Manufacturing" @if($user->business_stream=="Manufacturing") selected @endif>Manufacturing</option>
                                    <option value="Entertainment" @if($user->business_stream=="Entertainment") selected @endif>Entertainment</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="form-group">
                            <fieldset>
                                <select class="form-control dropdown" name="experience" required="">
                                    <option value="" selected="selected" disabled="disabled"> Your Experience</option>
                                    <option value="0 - 1 years" @if($user->experience=="0 - 1 years") selected @endif>0 - 1 years</option>
                                    <option value="1 - 2 years" @if($user->experience=="1 - 2 years") selected @endif>1 - 2 years</option>
                                    <option value="2 - 3 years" @if($user->experience=="2 - 3 years") selected @endif>2 - 3 years</option>
                                    <option value="3 - 5 years" @if($user->experience=="3 - 5 years") selected @endif>3 - 5 years</option>
                                    <option value="above 5 years" @if($user->experience=="above 5 years") selected @endif>above 5 years</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>                         
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Your Email *" name="Email"  id="Email" value="{{$user->email}}" disabled="" required="" />
                        </div>
                        <div class="form-group">
                            <input type="text" minlength="10" maxlength="10" name="phone" id="phone_no" class="form-control" placeholder="Your Phone *" value="{{$user->phone}}" required="" />
                        </div>
                        <div class="form-group">
                                <input type="text" class="form-control" id="city"  name="city" placeholder="Your City *" value="{{$user->city}}" required="" />
                        </div>
                        <div class="form-group">
                            <div class="maxl" style="padding-top: 10px">
                                <label class="radio inline"> 
                                    <input type="radio" name="gender" value="male" @if($user->gender=="male") checked @endif>
                                    <span> Male </span> 
                                </label>
                                <label class="radio inline"> 
                                    <input type="radio" name="gender" value="female" @if($user->gender=="female") checked @endif>
                                    <span>Female </span> 
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <fieldset>
                						    <select class="form-control dropdown" id="education" name="education" required="">
                    						    <option value="" selected="selected" disabled="disabled">Your Education</option>
                    						    <option value="No formal education" @if($user->education=="No formal education") selected @endif>No formal education</option>
                    						    <option value="Primary education" @if($user->education=="Primary education") selected @endif>Primary education</option>
                    						    <option value="Secondary education" @if($user->education=="Secondary education") selected @endif>Secondary education or high school</option>
                    						    <option value="GED" @if($user->education=="GED") selected @endif>GED</option>
                    						    <option value="Vocational qualification" @if($user->education=="Vocational qualification") selected @endif>Vocational qualification</option>
                    						    <option value="Bachelors degree" @if($user->education=="Bachelors degree") selected @endif>Bachelors degree</option>
                    						    <option value="Masters degree" @if($user->education=="Masters degree") selected @endif>Masters degree</option>
                    						    <option value="Doctorate or higher" @if($user->education=="Doctorate or higher") selected @endif>Doctorate or higher</option>
            						        </select>
            						    </fieldset>
                        </div>
                        <div class="form-group">
                            <fieldset>
                                <select class="form-control dropdown" id="skills" name="skills" required="">
                                    <option value="" selected="selected" disabled="disabled">Select Skills</option>
                                    <option value="PHP" @if($user->skills=="PHP") selected @endif >PHP</option>
                                    <option value="JAVA" @if($user->skills=="JAVA") selected @endif >JAVA</option>
                                    <option value="AWS" @if($user->skills=="AWS") selected @endif >AWS</option>
                                </select>
                            </fieldset>  
                        </div>
                        <input type="submit" style="background-color: #D39E00;color: #fff; float: right;" class="btn btnRegister"   value="Update"/>
                    </div>
                </div>
            </form> 
        </div>
    </div>
@endsection