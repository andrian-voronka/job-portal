<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>Jinder</title>




  <!-- Bootstrap core CSS -->
  <link href="{{url('public')}}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{url('public')}}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>


    <link href="{{url('public')}}/css/agency.min.css" rel="stylesheet">
 <link href="{{url('public')}}/css/swipe.css" rel="stylesheet">
  <link href="{{url('public')}}/css/style.css" rel="stylesheet">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">


</style>
</head>

<body id="page-top">


   @include('theme.header')

 
     @yield('content')

   @include('theme.footer')


  <!---        Modals - Employeer -->
 <!-- Modal -->
<!-- Modal HTML Markup -->
<div id="EmployeerModal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             
            <div class="modal-body">
                
                <div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p>You are 30 seconds away from Jinder !</p>
                        
                    </div>
                    <div class="col-md-9 register-right">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Job Seeker</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Employeer</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Apply as a Employee</h3>
                                <form method="POST" action="{{url('register')}}">
                                    <div class="row register-form">
                                        @csrf
                                        <input type="hidden" name="role" value="user">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="First Name *" name="first_name" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Last Name *" name="last_name" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" placeholder="Password *" name="password" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control"  placeholder="Confirm Password *" name="password_confirmation" value="" required="" />
                                            </div>                                      
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Your Email *" name="email" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" minlength="10" maxlength="10" name="phone" class="form-control" placeholder="Your Phone *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="city" placeholder="Your City *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <div class="maxl" style="padding-top: 10px">
                                                    <label class="radio inline"> 
                                                        <input type="radio" name="gender" value="male" required="">
                                                        <span> Male </span> 
                                                    </label>
                                                    <label class="radio inline"> 
                                                        <input type="radio" name="gender" value="female" required="">
                                                        <span>Female </span> 
                                                    </label>
                                                </div>
                                            </div>
                                            <input type="submit" class="btnRegister"  value="Register"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <h3  class="register-heading">Apply as a Hirer</h3>
                                <form method="POST" action="{{url('register')}}">
                                    <div class="row register-form">
                                        @csrf
                                        <input type="hidden" name="role" value="company">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name *" value="" required=""  />
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" placeholder="Email *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="text" maxlength="10" minlength="10" name="phone" class="form-control" placeholder="Phone *" value="" required="" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password" placeholder="Password *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password *" value="" required="" />
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" name="question">
                                                    <option class="hidden"  selected disabled>Please select your Sequrity Question</option>
                                                    <option value="What is your Birthdate?">What is your Birthdate?</option>
                                                    <option value="What is Your old Phone Number">What is Your old Phone Number</option>
                                                    <option value="What is your Pet Name?">What is your Pet Name?</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="answer" placeholder="`Answer *" value="" required="" />
                                            </div>
                                            <input type="submit" class="btnRegister"  value="Register"/>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="LoginModal" class="modal fade">
     <div class="modal-dialog" role="document" style="width: 30%">
        <div class="modal-content">
            <div class="modal-body" >
                <div class="container register">
                    <div class="row">
                        <div class="col-md-1 register-left">
                             
                        </div>
                        <div class="col-md-10 register-right">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <form method="POST" action="{{url('login')}}">
                                        @csrf
                                        <h3 class="register-heading">Login</h3>
                                        <div class="row register-form">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" placeholder="Username or Email *" name="email" value="" required="" />
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" name="password" placeholder="Password *" value="" required=""/>
                                                </div>
                                          
                                                  <input style="align-items: center;" type="submit" class="btnRegister"  value="Login"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div><!-- /.modal-content --> 
</div>





  <script type="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.4/hammer.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script src="{{url('public')}}/vendor/jquery/jquery.min.js"></script>
  <script src="{{url('public')}}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="{{url('public')}}/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact form JavaScript -->
  <script src="{{url('public')}}/js/jqBootstrapValidation.js"></script>
  <script src="{{url('public')}}/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="{{url('public')}}/js/agency.min.js"></script>
  <script src="https://hammerjs.github.io/dist/hammer.js"></script>
  <!-- <script src="{{url('public')}}/js/swipe.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

@yield('script')
    

</body>

</html>
