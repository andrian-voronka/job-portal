 <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top" style="color: #FED600">Jinder</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          @auth
          @if(Auth::user()->role == "company")
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger"  href="{{url('postJob')}}">Employeers Service</a>
          </li>
          @endif
          @if(Auth::user()->role=="user")
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger"  href="{{url('')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger"  href="{{url('matchedjobs')}}">Matched Jobs</a>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{url('about')}}">About</a>
          </li>
         
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{url('logout')}}">Logout</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="{{url('login')}}">Sign In</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" data-toggle="modal" data-target="#EmployeerModal" href="#">Sign Up</a>
          </li>
          @endauth
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
<section class="search-banner text-white py-3 form-arka-plan" id="search-banner">
    <div class="container py-5 my-5">
        <div class="row text-center pb-4">
            <div class="col-md-12">
                <h2 class="text-white siyah-cerceve">Search your dream job on Jinder</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card acik-renk-form">
                    <form method="get" action="{{url('findJob')}}">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <select class="form-control dropdown" name="business_stream" required="">
                                            <option value="" selected="selected" disabled="disabled">Business Stream</option>
                                            <option value="Aerospace">Aerospace </option>
                                            <option value="Transport">Transport </option>
                                            <option value="Computer ,IT">Computer ,IT</option>
                                            <option value="Telecommunication">Telecommunication </option>
                                            <option value="Agriculture">Agriculture </option>
                                            <option value="Construction">Construction  </option>
                                            <option value="Education">Education  </option>
                                            <option value="Pharmaceutical">Pharmaceutical</option>
                                            <option value="Food">Food</option>
                                            <option value="Health care">Health care</option>
                                            <option value="Hospitality">Hospitalit</option>
                                            <option value="News Media">News Medi</option>
                                            <option value="Energy">Energy</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Entertainment">Entertainment</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <select class="form-control dropdown" name="skills" required="">
                                            <option value="" selected="selected" disabled="disabled">Select Skills</option>
                                            <option value="PHP">PHP</option>
                                            <option value="JAVA">JAVA</option>
                                            <option value="AWS">AWS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <?php 
                                            $path = public_path() . "\canadian_cities.json";
                                            $cities = file_get_contents($path); 
                                            $citys = json_decode($cities);
                                         ?>
                                        <select class="form-control dropdown" name="city" required="">
                                            <option value="" selected="selected" disabled="disabled">Select City</option>
                                            @foreach($citys as $city)
                                              <option value="{{$city->name}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group ">
                                        <input type="text" class="form-control" placeholder="Keywords">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-warning  pl-5 pr-5">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
