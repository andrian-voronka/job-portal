@extends('theme.default')

@section('content')

<div class="container" style="height: 800px;padding-top: 30px">
    <h4 style="text-align: center;">Employeer Services.</h4>
  <hr>
  <div class="row">
    <div class="col-md-2 mb-3">
        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="jobs-tab" data-toggle="tab" href="#jobs" role="tab" aria-controls="jobs" aria-selected="true">Matched Jobs</a>
          </li>
        </ul>
    </div>
    <!-- /.col-md-4 -->
    <div class="col-md-10">
      <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="jobs" role="tabpanel" aria-labelledby="jobs-tab">
              <h2>Matched Jobs</h2>
              <div class="row">
                  <table class="table table-stripped">
                    <thead>
                      <tr>
                        <th>Job Title</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Business Stream</th>
                        <th>Skill</th>
                        <th>Experience</th>
                        <th>Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($employees as $employee)
                        @if($employee->user_id == $currentUser->_id)
                          @foreach($applications as $application)
                            @if($employee->user_id == $application->user_id)
                              @foreach($jobs as $job)
                                @if($application->job_id == $job->_id && $employee->company_id == $job->user_id)
                                  @foreach($users as $user)
                                    @if($user->_id == $employee->company_id)
                                    <tr>
                                      <td>{{$job->title}}</td>
                                      <td>{{$user->first_name}}</td>
                                      <td>{{$user->last_name}}</td>
                                      <td>{{$job->business_stream}}</td>
                                      <td>{{$job->skills}}</td>
                                      <td>{{$job->experience}}</td>
                                      <td>{{$job->description}}</td>
                                      <td><a href="">Action</a></td>
                                    </tr>
                                    @endif
                                  @endforeach
                                @endif
                              @endforeach
                            @endif
                          @endforeach
                        @endif
                      @endforeach
                    </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
    <!-- /.col-md-8 -->
  </div>
  
  
  
</div>
<!-- /.container -->

@endsection