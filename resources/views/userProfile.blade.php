@extends('theme.default')
@section('content')

 

<input type="hidden" name="param" id="param" value="{{$Email}}">
<div class="container" style="padding: 20px">

<div style="padding: 30px">
  <h5 style="text-align: center;">Update your profile</h5>
	
</div>

<div  style="padding-bottom: 50px">
	<form method="POST" action="{{ action('AccountController@updateProfile') }}">
	 <div class="row ">

         @csrf
        <div class="col-md-6">
             <div class="form-group">
               <input type="text" class="form-control" placeholder="First Name *" name="first_name" id="first_name" value="" />
             </div>
             <div class="form-group">
                <input type="text" class="form-control" placeholder="Last Name *" name="last_name" id="last_name" value="" />
             </div>
             <div class="form-group">
                <input type="password" class="form-control" placeholder="Password *" name="password" id="password"  value="" />
             </div>
             <div class="form-group">
                 <input type="password" class="form-control"  placeholder="Confirm Password *" name="retypePassword" value="" />
             </div>
             <div class="form-group">
                  <input class="date form-control" name="dob" type="date">
             </div>
                                     

             <div class="form-group">
                       <fieldset>
                             <select class="form-control dropdown" id="business_stream" name="business_stream">
                                      <option value="" selected="selected" disabled="disabled">Business Stream</option>

					      <option value="Aerospace">Aerospace </option>
					    <option value="Transport">Transport </option>
					    <option value="Computer ,IT">Computer ,IT</option>
					    <option value="Telecommunication">Telecommunication </option>
					    <option value="Agriculture">Agriculture </option>
					        <option value="Construction">Construction  </option>
					    <option value="Education">Education  </option>
					            <option value="Pharmaceutical">Pharmaceutical   </option>
					    <option value="Food">Food   </option>
					            <option value="Health care">Health care  </option>
					    <option value="Hospitality">Hospitality   </option>

					               <option value="News Media">News Media  </option>
					    <option value="Energy">Energy    </option>


					               <option value="Manufacturing">Manufacturing   </option>
					    <option value="Entertainment">Entertainment    </option>
					    
					  </select>
                      </fieldset>
            </div>

            <div class="form-group">
                <fieldset>
  
					    <select class="form-control dropdown" id="education" name="Experience">
					    <option value="" selected="selected" disabled="disabled"> Your Experience</option>
					    <option value="0 - 1 years">0 - 1 years</option>
					    <option value="1 - 2 years">1 - 2 years</option>
					    <option value="2 - 3years">2 - 3years</option>
					    <option value="3 - 5 years">3 - 5 years</option>
					    <option value="above 5 years">above 5 years</option>
					   
					  </select>
                </fieldset>
            </div>

        </div>                         
        <div class="col-md-6">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Your Email *" name="Email"  id="Email" value="" />
                </div>
                <div class="form-group">
                    <input type="text" minlength="10" maxlength="10" name="phone_no" id="phone_no" class="form-control" placeholder="Your Phone *" value="" />
                </div>
                <div class="form-group">
                        <input type="text"  name="txtEmpPhone" class="form-control" id="city"  name="city" placeholder="Your City *" value="" />
                </div>
                <div class="form-group">
                    <div class="maxl" style="padding-top: 10px">
                        <label class="radio inline"> 
                            <input type="radio" name="gender" value="male" checked>
                            <span> Male </span> 
                        </label>
                        <label class="radio inline"> 
                            <input type="radio" name="gender" value="female">
                            <span>Female </span> 
                        </label>
                    </div>
                </div>
                 <div class="form-group">
                       <fieldset>
						  
						    <select class="form-control dropdown" id="education" name="education">
						    <option value="" selected="selected" disabled="disabled">Your Education</option>
						    <option value="No formal education">No formal education</option>
						    <option value="Primary education">Primary education</option>
						    <option value="Secondary education">Secondary education or high school</option>
						    <option value="GED">GED</option>
						    <option value="Vocational qualification">Vocational qualification</option>
						    <option value="Bachelor's degree">Bachelor's degree</option>
						    <option value="Master's degree">Master's degree</option>
						    <option value="Doctorate or higher">Doctorate or higher</option>
						  </select>
						</fieldset>
                </div>


   
	              <div class="form-group">
	                    <input type="text"  name="skills" class="form-control" name="skills" placeholder="Your skills *" value="" />    
	                </div>


	                <input type="submit" style="background-color: #D39E00;color: #fff; float: right;" class="btn btnRegister"   value="Update"/>
	    </div>
	         
	</div>
	</form> 
</div>
 
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
alert('{{ url('ajaxRequest')}}');
	    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
          $.ajax({

           type:'GET',

          url: "{{ url('getProfile')}}",

           data: {Email:$("#param").val()},

           success:function(data){
          
           var array = $.map(data.success, function(value, index) {
    return [value];});
          console.log(array);

          $.each(array, function(i, v) {
            if (v.Email == $("#param").val()) {
            	alert(v.first_name);
                 $("#first_name").val( v.first_name);
                 $("#last_name").val( v.last_name);
                 $("#password").val( v.password);
                 $("#city").val( v.city);
                 $("#Email").val( v.Email);
                 $("#phone_no").val( v.Email);
                  return;
          }
});

       }

        });
  
 
</script>  


@endsection