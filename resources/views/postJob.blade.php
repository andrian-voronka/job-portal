@extends('theme.default')

@section('content')

<div class="container" style="height: 800px;padding-top: 30px">
    <h4 style="text-align: center;">Employeer Services.</h4>
  <hr>
  <div class="row">
    <div class="col-md-2 mb-3">
        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Post Job</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Matched Profile</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#jobs" role="tab" aria-controls="jobs" aria-selected="false">Active Jobs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user" aria-selected="false">Users</a>
          </li>
        </ul>
    </div>
    <!-- /.col-md-4 -->
    <div class="col-md-10">
      <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <form style="padding: 15px" method="POST" action="{{url('postJob')}}">
                @csrf
                <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">Stream</p></div>
                  <div class="col-md-9">
                    <select class="form-control dropdown" style="width: 50%" id="sel1" name="business_stream" required="">
                        <option value="" selected="selected" disabled="disabled">Business Stream</option>
                        <option value="Aerospace">Aerospace </option>
                        <option value="Transport">Transport </option>
                        <option value="Computer ,IT">Computer ,IT</option>
                        <option value="Telecommunication">Telecommunication </option>
                        <option value="Agriculture">Agriculture </option>
                        <option value="Construction">Construction  </option>
                        <option value="Education">Education  </option>
                        <option value="Pharmaceutical">Pharmaceutical</option>
                        <option value="Food">Food</option>
                        <option value="Health care">Health care</option>
                        <option value="Hospitality">Hospitalit</option>
                        <option value="News Media">News Medi</option>
                        <option value="Energy">Energy</option>
                        <option value="Manufacturing">Manufacturing</option>
                        <option value="Entertainment">Entertainment</option>
                    </select> 
                   </div>
                </div>
                 <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">Skill</p></div>
                  <div class="col-md-9">
                     <select class="form-control dropdown" style="width: 50%" id="sel1" name="skills" required="">
                          <option value="" selected="selected" disabled="disabled">Select Skills</option>
                          <option value="PHP">PHP</option>
                          <option value="JAVA">JAVA</option>
                          <option value="AWS">AWS</option>
                      </select>
                   </div>
                </div>
                <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">Experience</p></div>
                  <div class="col-md-9">
                    <select class="form-control dropdown" style="width: 50%" id="sel1" name="experience" required="">
                          <option value="" selected="selected" disabled="disabled"> Your Experience</option>
                          <option value="0 - 1 years">0 - 1 years</option>
                          <option value="1 - 2 years">1 - 2 years</option>
                          <option value="2 - 3 years">2 - 3 years</option>
                          <option value="3 - 5 years">3 - 5 years</option>
                          <option value="above 5 years">above 5 years</option>
                      </select>
                   </div>
                </div>
                <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">City</p></div>
                  <div class="col-md-9">
                     <select class="form-control dropdown" style="width: 50%" id="sel1" name="city" required="">
                          <option value="" selected="selected" disabled="disabled">Select City</option>
                          @foreach($citys as $city)
                            <option value="{{$city->name}}">{{$city->name}}</option>
                          @endforeach
                      </select>
                   </div>
                </div>
                <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">Title</p></div>
                  <div class="col-md-9">
                    <input type="text-align" multiple="true" class="form-control" style="width: 50%" name="title">
                   </div>
                </div>
                 <div class="row">
                  <div class="col-md-3"><p style="text-align: right;padding-top: 3px">Description</p></div>
                  <div class="col-md-9">
                    <input type="text-align" multiple="true" class="form-control" style="width: 50% ; height: 100px"  name="description">
                   </div>

                   <div style="width: 63%;padding-top: 10px">
                     <input type="submit" style="float: right;" class="btn btn-primary" value="Save"  name="">
                   </div>
                </div>

              </form>
          </div>

          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <h2>Matched Profiles</h2>
              <div class="row">
                  <table class="table table-stripped">
                    <thead>
                      <tr>
                        <th>Job Title</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Business Stream</th>
                        <th>Skill</th>
                        <th>Experience</th>
                        <th>City</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($employees as $employee)
                        @if($employee->company_id == $currentUser->_id)
                          @foreach($applys as $application)
                            @if($employee->user_id == $application->user_id)
                              @foreach($jobs as $job)
                                @if($application->job_id == $job->_id && $employee->company_id == $job->user_id)
                                  @foreach($users as $user)
                                    @if($user->_id == $employee->user_id)
                                    <tr>
                                      <td>{{$job->title}}</td>
                                      <td>{{$user->first_name}}</td>
                                      <td>{{$user->last_name}}</td>
                                      <td>{{$user->business_stream}}</td>
                                      <td>{{$user->skills}}</td>
                                      <td>{{$user->experience}}</td>
                                      <td>{{$user->city}}</td>
                                      <td><a href="">Action</a></td>
                                    </tr>
                                    @endif
                                  @endforeach
                                @endif
                              @endforeach
                            @endif
                          @endforeach
                        @endif
                      @endforeach
                    </tbody>
                  </table>
              </div>
          </div>

          <div class="tab-pane fade" id="jobs" role="tabpanel" aria-labelledby="jobs-tab">
              <h2>Active Jobs</h2>
              <div class="row">
                  <table class="table table-stripped">
                    <thead>
                      <tr>
                        <th>Job Title</th>
                        <th>Skills</th>
                        <th>Experience</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($jobs as $job)
                      @if($job->user_id == $currentUser->_id)
                        <tr>
                          <td>{{$job->title}}</td>
                          <td>{{$job->skills}}</td>
                          <td>{{$job->experience}}</td>
                          <td>{{$job->description}}</td>
                        </tr>
                      @endif
                    @endforeach
                    </tbody>
                  </table>
              </div>
          </div>

          <div class="tab-pane fade" id="user"  role="tabpanel" aria-labelledby="user-tab">
              <h2>User Profiles</h2>
              <div class="row">
                <div class="tinder">
                  <div class="tinder--status">
                    <i class="fa fa-remove"></i>
                    <i class="fa fa-heart"></i>
                  </div>
                  <div class="tinder--cards">
                      @foreach($users as $user)
                          <div class="tinder--card">
                            <img src="https://placeimg.com/600/300/people">
                            <h3>{{$user->first_name." ".$user->last_name}}</h3>
                            <p><span class="badge badge-primary badge-pill" style="float: left">Skills</span> {{$user->skills}}</p>
                            <p><span class="badge badge-primary badge-pill" style="float: left">Experience</span> {{$user->experience}}</p>
                            <p><span class="badge badge-primary badge-pill" style="float: left">City</span> {{$user->city}}</p>
                            <input type="hidden" id="userid" value="{{$user->_id}}">
                          </div>
                      @endforeach
                  </div>

                  <div class="tinder--buttons">
                      <span class="badge badge-danger badge-pill">Reject</span>
                      <span><i class="fa fa-arrow-left" style="padding-right: 20px;"></i></span>
                      <span><i class="fa fa-arrow-right"></i></span>
                      <span class="badge badge-success badge-pill">Apply</span><br>
                      <span class="badge badge-info badge-pill">Swipe on Image</span>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
    <!-- /.col-md-8 -->
  </div>
  
  
  
</div>
<!-- /.container -->

@endsection

@section('script')
    <script type="text/javascript">
        'use strict';

        var tinderContainer = document.querySelector('.tinder');
        var allCards = document.querySelectorAll('.tinder--card');
        var nope = document.getElementById('nope');
        var love = document.getElementById('love');

        function initCards(card, index) {
          var newCards = document.querySelectorAll('.tinder--card:not(.removed)');

          newCards.forEach(function (card, index) {
            card.style.zIndex = allCards.length - index;
            card.style.transform = 'scale(' + (20 - index) / 20 + ') translateY(-' + 30 * index + 'px)';
            card.style.opacity = (10 - index) / 10;
          });
          
          tinderContainer.classList.add('loaded');
        }

        initCards();

        allCards.forEach(function (el) {
          var hammertime = new Hammer(el);
          hammertime.on('pan', function (event) {
            el.classList.add('moving');
          });

          hammertime.on('pan', function (event) {
            if (event.deltaX === 0) return;
            if (event.center.x === 0 && event.center.y === 0) return;
            tinderContainer.classList.toggle('tinder_love', event.deltaX > 0);
            tinderContainer.classList.toggle('tinder_nope', event.deltaX < 0);

            var xMulti = event.deltaX * 0.03;
            var yMulti = event.deltaY / 80;
            var rotate = xMulti * yMulti;

            event.target.style.transform = 'translate(' + event.deltaX + 'px, ' + event.deltaY + 'px) rotate(' + rotate + 'deg)';
          });

          hammertime.on('panend', function (event) {
            el.classList.remove('moving');
            tinderContainer.classList.remove('tinder_love');
            tinderContainer.classList.remove('tinder_nope');
            if($.trim(event.type)=="panend" && $.trim(event.additionalEvent)=="panright")
            {
                @auth
                    applyJob(event.target.lastElementChild.defaultValue);
                @else
                    window.location.href = "{{url('login')}}";
                @endauth
                //console.log(event.target.lastElementChild.defaultValue);
            }
            var moveOutWidth = document.body.clientWidth;
            var keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;

            event.target.classList.toggle('removed', !keep);

            if (keep) {
              event.target.style.transform = '';
            } else {
              var endX = Math.max(Math.abs(event.velocityX) * moveOutWidth, moveOutWidth);
              var toX = event.deltaX > 0 ? endX : -endX;
              var endY = Math.abs(event.velocityY) * moveOutWidth;
              var toY = event.deltaY > 0 ? endY : -endY;
              var xMulti = event.deltaX * 0.03;
              var yMulti = event.deltaY / 80;
              var rotate = xMulti * yMulti;

              event.target.style.transform = 'translate(' + toX + 'px, ' + (toY + event.deltaY) + 'px) rotate(' + rotate + 'deg)';
              initCards();
            }
          });
        });

        function createButtonListener(love) {
          return function (event) {
            var cards = document.querySelectorAll('.tinder--card:not(.removed)');
            var moveOutWidth = document.body.clientWidth * 1.5;

            if (!cards.length) return false;

            var card = cards[0];

            card.classList.add('removed');

            if (love) {
              card.style.transform = 'translate(' + moveOutWidth + 'px, -100px) rotate(-30deg)';
            } else {
              card.style.transform = 'translate(-' + moveOutWidth + 'px, -100px) rotate(30deg)';
            }

            initCards();

            event.preventDefault();
          };
        }

        var nopeListener = createButtonListener(false);
        var loveListener = createButtonListener(true);

        nope.addEventListener('click', nopeListener);
        love.addEventListener('click', loveListener);

    </script>

    <script type="text/javascript">
        function applyJob(userId)
        {
            /*$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });*/

            $.ajax({
                url: "{{url('add-employee')}}",
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { user_id : userId },
                success: function(data){
                    alert(data);
                }
            });
        }
    </script>
@endsection