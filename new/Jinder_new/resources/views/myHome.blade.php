@extends('theme.default')



@section('content')
 
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center" style="padding-top: 10px">
          
          <h3 class="section-subheading text-muted">Our Services</h3>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-search fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Search Millions of Jobs</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-suitcase fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Top Careers</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
        <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-lock fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading">Web Security</h4>
          <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
        </div>
      </div>
    </div>


<div class="container-fluid" style="border-top: 2px">
	<div class="row" style="height: 40px;background-color: #E0A800">
		<div class="col-sm-3 px-1   min-vh-100">
            <h4 style="text-align: center; padding-top: 7px;font-family: Roboto Slab">Business Stream</h4>
        </div>
	</div>
    <div class="row" >
        <div class="col-sm-3 px-1   min-vh-100">
            <ul class="list-group">
              <li class="list-group-item d-flex justify-content-between align-items-center">
              Aerospace
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Transport
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Computer ,IT
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Telecommunication
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Agriculture
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Construction
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Education
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Pharmaceutical
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Food
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Health care
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Hospitalit
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              News Medi
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Energy
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Manufacturing
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
              Entertainment
              <span class="badge badge-primary badge-pill">0</span>
            </li>
            </ul>
        </div>
        <div class="col-md-9" id="main">
           <div class="tinder">
  <div class="tinder--status">
    <i class="fa fa-remove"></i>
    <i class="fa fa-heart"></i>
  </div>

  <div class="tinder--cards">
    <?php if ($currentUser == null ) $currentUserRole = 'user';
    else $currentUserRole = $currentUser->role;
    if ($currentUserRole == 'user') { ?>
      @foreach($jobs as $job)
        <?php if(count($applications) == 0) { ?>
          <div class="tinder--card">
            <img src="https://placeimg.com/600/300/people">
            <h3 data-id="{{$job->_id}}">{{$job->title}}</h3>
            <p><span class="badge badge-primary badge-pill" style="float: left">Skills</span> {{$job->skills}}</p>
            <p><span class="badge badge-primary badge-pill" style="float: left">Experience</span> {{$job->experience}}</p>
            <p><span class="badge badge-primary badge-pill" style="float: left">Description</span> {{$job->description}}</p>
            <input type="hidden" id="jobid" value="{{$job->_id}}">
          </div>
        <?php } else { ?>
          @foreach($applications as $application)
            <?php if ($currentUser == null || $application->job_id != $currentUser->_id) { ?>
              <div class="tinder--card">
                <img src="https://placeimg.com/600/300/people">
                <h3 data-id="{{$job->_id}}">{{$job->title}}</h3>
                <p><span class="badge badge-primary badge-pill" style="float: left">Skills</span> {{$job->skills}}</p>
                <p><span class="badge badge-primary badge-pill" style="float: left">Experience</span> {{$job->experience}}</p>
                <p><span class="badge badge-primary badge-pill" style="float: left">Description</span> {{$job->description}}</p>
                <input type="hidden" id="jobid" value="{{$job->_id}}">
              </div>
            <?php } ?>
          @endforeach
        <?php } ?>
      @endforeach
    <?php } else if ($currentUserRole == 'company' ) { ?>
      @foreach($users as $user)
        <?php if ($user->role == 'user') { 
          if (count($applications) != 0) { ?>
            @foreach($applications as $application)
              <?php
              if (!($application->user_id == $currentUser->_id && $application->job_id == $user->_id)) { ?>
                <div class="tinder--card">
                  <img src="https://placeimg.com/600/300/people">
                  <h3 data-id="{{$user->_id}}">{{$user->first_name . " " . $user->last_name}}</h3>
                  <p><span class="badge badge-primary badge-pill" style="float: left">City</span> {{$user->city}}</p>
                  <p><span class="badge badge-primary badge-pill" style="float: left">Stream</span> {{$user->business_stream}}</p>
                  <p><span class="badge badge-primary badge-pill" style="float: left">Skill</span> {{$user->skills}}</p>
                  <p><span class="badge badge-primary badge-pill" style="float: left">Experience</span> {{$user->experience}}</p>
                  <p><span class="badge badge-primary badge-pill" style="float: left">Education</span> {{$user->education}}</p>
                  <input type="hidden" id="userid" value="{{$user->_id}}">
                </div>
              <?php }?>
            @endforeach
          <?php } else { ?>
            <div class="tinder--card">
              <img src="https://placeimg.com/600/300/people">
              <h3 data-id="{{$user->_id}}">{{$user->first_name . " " . $user->last_name}}</h3>
              <p><span class="badge badge-primary badge-pill" style="float: left">City</span> {{$user->city}}</p>
              <p><span class="badge badge-primary badge-pill" style="float: left">Stream</span> {{$user->business_stream}}</p>
              <p><span class="badge badge-primary badge-pill" style="float: left">Skill</span> {{$user->skills}}</p>
              <p><span class="badge badge-primary badge-pill" style="float: left">Experience</span> {{$user->experience}}</p>
              <p><span class="badge badge-primary badge-pill" style="float: left">Education</span> {{$user->education}}</p>
              <input type="hidden" id="userid" value="{{$user->_id}}">
            </div>
          <?php }}?>
        @endforeach
    <?php } ?>
  </div>

  <div class="tinder--buttons">
    <span class="badge badge-danger badge-pill">Reject</span>
    <span><i class="fa fa-arrow-left" style="padding-right: 20px;"></i></span>
    <span><i class="fa fa-arrow-right"></i></span>
    <span class="badge badge-success badge-pill">Apply</span><br>
    <span id="reject"><i class="fas fa-thumbs-down" ></i></span>
    <span class="badge badge-info badge-pill">Swipe on Image</span>
    <span id="apply"><i class="fas fa-thumbs-up"></i></span>
  </div>
</div>

        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        'use strict';

        var tinderContainer = document.querySelector('.tinder');
        var allCards = document.querySelectorAll('.tinder--card');
        var nope = document.getElementById('nope');
        var love = document.getElementById('love');
        var reject = document.getElementById('reject');
        var apply = document.getElementById('apply');

        function initCards(card, index) {
          var newCards = document.querySelectorAll('.tinder--card:not(.removed)');

          newCards.forEach(function (card, index) {
            card.style.zIndex = allCards.length - index;
            card.style.transform = 'scale(' + (20 - index) / 20 + ') translateY(-' + 30 * index + 'px)';
            card.style.opacity = (10 - index) / 10;
          });
          
          tinderContainer.classList.add('loaded');
        }

        initCards();

        allCards.forEach(function (el) {
          var hammertime = new Hammer(el);
          hammertime.on('pan', function (event) {
            el.classList.add('moving');
          });

          hammertime.on('pan', function (event) {
            if (event.deltaX === 0) return;
            if (event.center.x === 0 && event.center.y === 0) return;
            tinderContainer.classList.toggle('tinder_love', event.deltaX > 0);
            tinderContainer.classList.toggle('tinder_nope', event.deltaX < 0);

            var xMulti = event.deltaX * 0.03;
            var yMulti = event.deltaY / 80;
            var rotate = xMulti * yMulti;

            event.target.style.transform = 'translate(' + event.deltaX + 'px, ' + event.deltaY + 'px) rotate(' + rotate + 'deg)';
          });

          hammertime.on('panend', function (event) {
            el.classList.remove('moving');
            tinderContainer.classList.remove('tinder_love');
            tinderContainer.classList.remove('tinder_nope');
            if($.trim(event.type)=="panend" && $.trim(event.additionalEvent)=="panright")
            {
                @auth
                    applyJob(event.target.lastElementChild.defaultValue);
                @else
                    window.location.href = "{{url('login')}}";
                @endauth
                console.log(event.target.lastElementChild.defaultValue);
            }
            var moveOutWidth = document.body.clientWidth;
            var keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;

            event.target.classList.toggle('removed', !keep);

            if (keep) {
              event.target.style.transform = '';
            } else {
              var endX = Math.max(Math.abs(event.velocityX) * moveOutWidth, moveOutWidth);
              var toX = event.deltaX > 0 ? endX : -endX;
              var endY = Math.abs(event.velocityY) * moveOutWidth;
              var toY = event.deltaY > 0 ? endY : -endY;
              var xMulti = event.deltaX * 0.03;
              var yMulti = event.deltaY / 80;
              var rotate = xMulti * yMulti;

              event.target.style.transform = 'translate(' + toX + 'px, ' + (toY + event.deltaY) + 'px) rotate(' + rotate + 'deg)';
              initCards();
            }
          });
        });

        function createButtonListener(love) {
          console.log(love);
          return function (event) {
            var cards = document.querySelectorAll('.tinder--card:not(.removed)');
            var moveOutWidth = document.body.clientWidth * 0.8;

            if (!cards.length) return false;

            var card = cards[0];

            card.classList.add('removed');

            if (love) {
              card.style.transform = 'translate(' + moveOutWidth + 'px, -100px) rotate(-30deg)';
            } else {
              card.style.transform = 'translate(-' + moveOutWidth + 'px, -100px) rotate(30deg)';
            }

            initCards();

            event.preventDefault();
          };
        }

        var nopeListener = createButtonListener(false);
        var loveListener = createButtonListener(true);

        nope.addEventListener('click', nopeListener);
        love.addEventListener('click', loveListener);

    </script>

    <script type="text/javascript">
        function applyJob(jobId)
        {
            /*$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });*/

            $.ajax({
                url: "{{url('apply-for-job')}}",
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { job_id : jobId },
                success: function(data){
                    $("#employees").html(data);
                }
            });
        }
    </script>
@endsection