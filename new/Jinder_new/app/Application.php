<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Application extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Application';
     protected $guarded = [];
    protected $primarykey = "_id";
    
     protected $fillable = [
        'job_id' ,
        'user_id',
    ];

    public function job()
    {
        $this->belongsTo('App\JobPost','_id','job_id');
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }

}
