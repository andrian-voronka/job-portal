<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User_type extends  Eloquent
{
	 protected $connection = 'mongodb';
     protected $collection = 'User_types';
    
     protected $fillable = [
        'id', 'user_type_name' 
    ];
    
}
