<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Business_Stream extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Business_Streams';
    
     protected $fillable = [
        'id', 'stream_name' 
    ];
}
