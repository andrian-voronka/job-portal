<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Item;
use App\JobPost;
use App\user_account;
use App\Application;
use App\Employee;
use Auth;
class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if($user->role=='user'){
            return redirect('profile');
        } else {
            return redirect('postJob');
        }
        //return view('home');
    }

    public function myHome()
    { 
        $data['jobs'] = JobPost::orderBy('created_at','DESC')->get();
        $data['users'] = user_account::orderBy('user_type_id', 'DESC')->get();
        $data['applications'] = Application::orderBy('user_id', 'DESC')->get();
        $data['employee'] = Employee::get();
        $data['currentUser'] = Auth::user();
        return view('myHome',$data);
    }

    public function about()
    { 
        // return View::make('myHome')->render();
          return view('about');

    }
    public function findJob(Request $request)
    { 
        $data = array();
        if(!empty($request->all()))
        {
            $get = $request->all();
            $data['jobs'] = JobPost::where($get)->orderBy('created_at','DESC')->get(); 
        }
        else
        {
            $data['jobs'] = JobPost::orderBy('created_at','DESC')->get();
        }
        $data['applications'] = Application::get();
        $data['currentUser'] = Auth::user();
        //echo "<pre>"; print_r($data); die("asd");
        return view('findJob',$data);
    }
    public function postJob()

    { 
        // return View::make('myHome')->render();
          return view('postJob');

    }
    public function matchedprofiles()

    { 
        // return View::make('myHome')->render();
          return view('matchedProfiles');

    }

}