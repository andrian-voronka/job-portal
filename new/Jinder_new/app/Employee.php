<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Employee extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Employee';
     protected $guarded = [];
    protected $primarykey = "_id";
    
     protected $fillable = [
        'company_id' ,
        'user_id',
    ];
}
