<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Skill_set extends Eloquent
{
     protected $connection = 'mongodb';
     protected $collection = 'Skill_sets';
    
     protected $fillable = [
        'id', 'skill_set_name' 
    ];
}
