<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@myHome');
Route::get('about', 'HomeController@about');
Route::get('matchedjobs', 'MatchedJobsController@index');
Route::get('findJob', 'HomeController@findJob');
//Route::get('postJob', 'HomeController@postJob');
Route::get('matchedprofiles', 'HomeController@matchedprofiles');
Route::post('createUserAccount', 'AccountController@store');
 //Route::get('userProfile', 'AccountController@getUserProfile');
//Route::get('getProfile', 'AccountController@getUserProfile');
Route::group(['middleware' => 'auth'], function () {
	Route::get('home', 'HomeController@index');

	Route::get('findJob', 'HomeController@findJob');

	// job post
	Route::get('postJob', 'JobPostController@index');
	Route::post('postJob', 'JobPostController@store');

	Route::post('apply-for-job', 'ApplicationController@store');
	Route::post('add-employee', 'ApplicationController@addEmployee');

	// get jobs
	Route::get('postJob', 'JobPostController@index');

	// user profile edit
	Route::get('profile', 'AccountController@getUserProfile');
	Route::post('profile', 'AccountController@updateUserProfile');
});

Route::post('updateProfile', 'AccountController@updateProfile');
Auth::routes();


